#### Detect palindrom string

String are given. Implement a function that can detect palindrom string. If given argument not an string - "Passed argument is not a string". If given string is empty - "String is empty". Also, case sensitive.

Exp:
- detectPalindrom(true) // "Passed argument is not a string"
- detectPalindrom('') // "String is empty"
- detectPalindrom("TARARAT") // 'This string is palindrom!'
- detectPalindrom("testtest")) // 'This string is not a palindrom!'

For run:

1. Clone project to a local repository
2. Run npm install
3. Implement function
4. Run npm test
<hr>