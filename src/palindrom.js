const detectPalindrom = (str) => {
  // Your implementation
  // Read README.md file, if you not understand what to do
  let reg = /\s+/g;

  if (typeof str === "string") {
    if (str === "") {
      return "String is empty";
    } else if (
      str
        .trim()
        .split(reg)
        .join("")
        .split("")
        .reverse()
        .join("")
        .toUpperCase() === str.trim().split(reg).join("").toUpperCase()
    ) {
      return "This string is palindrom!";
    } else {
      return "This string is not a palindrom!";
    }
  } else {
    return "Passed argument is not a string";
  }
};
module.exports = detectPalindrom;
